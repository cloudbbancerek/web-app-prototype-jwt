# What to do first?
* Configure database (src/main/resources/application.properties)
* Create user structure in database - "spring.jpa.hibernate.ddl-auto" option set to "update" (src/main/resources/application.properties).
* Create actual users to work with - alter init method in InitUsers class and trigger it in Application's main method (src/main/java/ct/Application.java).
* Run the Application (src/main/java/ct/Application.java)


# How to use/test it?
* Hit the 'http://localhost:8080/login' endpoint:
    * curl -X POST -d '{"username":"[username]", "password":"[password]"}' http://localhost:8080/login
* Server responds with JSON containing token.
* Hit the 'http://localhost:8080/test/me' endpoint with token passed via 'Authorization' header (header name is configurable in application.properties):
    * curl -X GET -H 'Authorization: Bearer [token]' http://localhost:8080/test/me
* You may get 403 code because of @PreAuthorize annotation on method associated with this endpoint.


# src/main/resources/application.properties
* database configuration
* spring.jpa.hibernate.ddl-auto - creates table structure when set to "update" (it's better to switch it to "validate")
* security.jwt-secret - key signing JWT
* security.jwt-expiration-time - JWT expiration time (in milliseconds)
* security.jwt-header-name - header which will contain JWT token [default: Authorization]
* security.jwt-header-prefix - prefix preceding JWT token (in header) [default: Bearer]


# src/main/java/ct/security

* SecurityConstants.java:
    * constants from application.properties

* MethodSecurityConfiguration.java:
    * allows @PreAuthorize annotations on methods

* WebSecurityConfiguration.java:
    * defines some basic beans such as PasswordEncoder
    * configuration of HTTP security
    * adds custom filters to authentication/authorization filter chain

* JWTAuthenticationFilter.java
    * creates JWT token after successful authentication

* JWTAuthorizationFilter.java
    * verifies if correct JWT token was passed in HTTP header


# User model
User has a collection of roles.
Role consists of a collection of privileges.
