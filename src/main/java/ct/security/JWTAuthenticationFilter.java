package ct.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import ct.api.user.UserHelper;
import ct.api.user.dto.UserDTO;
import ct.api.user.model.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private UserDetailsService userDetailsService;

    private UserHelper userHelper;

    private SecurityConstants securityConstants;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserDetailsService userDetailsService, UserHelper userHelper, SecurityConstants securityConstants) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.userHelper = userHelper;
        this.securityConstants = securityConstants;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            UserDTO userDTO = userHelper.getUserDTOFromInputStream(request.getInputStream());
            UserDetails user = userDetailsService.loadUserByUsername(userDTO.getUsername());
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user, userDTO.getPassword(), user.getAuthorities()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new BadCredentialsException("Bad credentials");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        User user = (User) authResult.getPrincipal();
        String token = createJwtToken(user);
        prepareResponseContainingJwtToken(response, token);
    }

    private String createJwtToken(User user) throws UserHelper.UserConversionException {
        return JWT.create()
                .withSubject(user.getUsername())
                .withClaim("user", userHelper.serializeUser(user))
                .withExpiresAt(new Date(System.currentTimeMillis() + securityConstants.getJwtExpirationTime()))
                .sign(Algorithm.HMAC512(securityConstants.getJwtSecret().getBytes()));
    }

    private void prepareResponseContainingJwtToken(HttpServletResponse response, String token) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.print("{'token':'" + token + "'}");
        writer.flush();
    }
}
