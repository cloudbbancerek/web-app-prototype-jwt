package ct.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "security")
public class SecurityConstants {

    @NotNull
    private String jwtSecret;
    private int jwtExpirationTime = 600000;
    private String jwtHeaderName = "Authorization";
    private String jwtHeaderPrefix = "Bearer";

    public String getJwtSecret() {
        return jwtSecret;
    }

    public void setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    public int getJwtExpirationTime() {
        return jwtExpirationTime;
    }

    public void setJwtExpirationTime(int jwtExpirationTime) {
        this.jwtExpirationTime = jwtExpirationTime;
    }

    public String getJwtHeaderName() {
        return jwtHeaderName;
    }

    public void setJwtHeaderName(String jwtHeaderName) {
        this.jwtHeaderName = jwtHeaderName;
    }

    public String getJwtHeaderPrefix() {
        return jwtHeaderPrefix;
    }

    public void setJwtHeaderPrefix(String jwtHeaderPrefix) {
        this.jwtHeaderPrefix = jwtHeaderPrefix;
    }
}
