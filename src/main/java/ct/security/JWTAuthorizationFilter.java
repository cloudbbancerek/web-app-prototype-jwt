package ct.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import ct.api.user.UserHelper;
import ct.api.user.model.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private UserHelper userHelper;

    private SecurityConstants securityConstants;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, UserHelper userHelper, SecurityConstants securityConstants) {
        super(authenticationManager);
        this.userHelper = userHelper;
        this.securityConstants = securityConstants;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = getJwtToken(request);

        if (token != null) {
            authenticateUserFromJwtToken(token);
        }

        chain.doFilter(request, response);
    }

    private String getJwtToken(HttpServletRequest request) {
        String header = request.getHeader(securityConstants.getJwtHeaderName());

        if (header == null) {
            return null;
        }

        if (!header.startsWith(securityConstants.getJwtHeaderPrefix())) {
            return null;
        }

        return header.replace(securityConstants.getJwtHeaderPrefix(), "").trim();
    }

    private void authenticateUserFromJwtToken(String token) {
        User user = getUserFromJwtToken(token);

        if (user != null) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(user, "N/A", user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    private User getUserFromJwtToken(String token) {
        try {
            DecodedJWT jwt = JWT.require(Algorithm.HMAC512(securityConstants.getJwtSecret().getBytes())).build().verify(token);
            String serializedUser = jwt.getClaim("user").asString();
            User user = userHelper.deserializeUser(serializedUser);
            return user;
        } catch (JWTVerificationException e) {
            e.printStackTrace();
            return null;
        } catch (UserHelper.UserConversionException e) {
            e.printStackTrace();
            return null;
        }
    }
}
