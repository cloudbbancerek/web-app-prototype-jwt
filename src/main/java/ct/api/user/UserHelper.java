package ct.api.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ct.api.user.dto.UserDTO;
import ct.api.user.model.User;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

@Component
public class UserHelper {

    private ObjectMapper objectMapper = new ObjectMapper();

    public String serializeUser(User user) throws UserConversionException {
        try {
            String userAsJson = objectMapper.writeValueAsString(user);
            return Base64.getEncoder().encodeToString(userAsJson.getBytes());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new UserConversionException("Cannot serialize user");
        }
    }

    public User deserializeUser(String serializedUser) throws UserConversionException {
        try {
            byte[] userAsJson = Base64.getDecoder().decode(serializedUser);
            return objectMapper.readValue(userAsJson, User.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new UserConversionException("Cannot deserialize user");
        }
    }

    public UserDTO getUserDTOFromInputStream(InputStream inputStream) throws IOException {
        return objectMapper.readValue(inputStream, UserDTO.class);
    }

    public static class UserConversionException extends IOException {

        public UserConversionException(String message) {
            super(message);
        }
    }
}
