package ct.api.user;

import ct.api.user.repository.PrivilegeRepository;
import ct.api.user.repository.RoleRepository;
import ct.api.user.repository.UserRepository;
import org.springframework.context.ConfigurableApplicationContext;

public class InitUsers {

    public static void init(ConfigurableApplicationContext context) {
        PrivilegeRepository privilegeRepository = (PrivilegeRepository) context.getBean("privilegeRepository");
        RoleRepository roleRepository = (RoleRepository) context.getBean("roleRepository");
        UserRepository userRepository = (UserRepository) context.getBean("userRepository");

        // create privileges + save them (privilegeRepository.save(p))
        // create roles      + save them (roleRepository.save(r))
        // create users      + save them (userRepository.save(u))
    }
}
