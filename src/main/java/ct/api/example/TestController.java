package ct.api.example;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @PreAuthorize("@permissionHandler.hasAllPrivileges('p1', 'p2') and @permissionHandler.hasAnyRole('r1', 'r2')")
    @GetMapping(value = "me")
    public void testMe() {
        System.out.println("It works!");
    }
}
